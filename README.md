Title
=====
ceshi extionston

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist keke/keke "*"
```

or add

```
"keke/keke": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \keke\AutoloadExample::widget(); ?>```
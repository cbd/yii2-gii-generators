<?php
/**
 * This is the template for generating a CRUD controller class file.
 */

use yii\db\ActiveRecordInterface;
use yii\helpers\StringHelper;


/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$controllerClass = StringHelper::basename($generator->controllerClass);
$modelClass = StringHelper::basename($generator->modelClass);
$searchModelClass = StringHelper::basename($generator->searchModelClass);
if ($modelClass === $searchModelClass) {
    $searchModelAlias = $searchModelClass . 'Search';
}

/* @var $class ActiveRecordInterface */
$class = $generator->modelClass;
$pks = $class::primaryKey();
$urlParams = $generator->generateUrlParams();
$actionParams = $generator->generateActionParams();
$actionParamComments = $generator->generateActionParamComments();

echo "<?php\n";
?>

namespace <?= StringHelper::dirname(ltrim($generator->controllerClass, '\\')) ?>;

use Yii;
use <?= ltrim($generator->modelClass, '\\') ?>;
<?php if (!empty($generator->searchModelClass)): ?>
use <?= ltrim($generator->searchModelClass, '\\') . (isset($searchModelAlias) ? " as $searchModelAlias" : "") ?>;
<?php else: ?>
use yii\data\ActiveDataProvider;
<?php endif; ?>
use <?= ltrim($generator->baseControllerClass, '\\') ?>;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * <?= $controllerClass ?> implements the CRUD actions for <?= $modelClass ?> model.
 */
class <?= $controllerClass ?> extends <?= StringHelper::basename($generator->baseControllerClass) . "\n" ?>
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all <?= $modelClass ?> models.
     * @return mixed
     */
    public function actionIndex()
    {
<?php if (!empty($generator->searchModelClass)): ?>
        $searchModel = new <?= isset($searchModelAlias) ? $searchModelAlias : $searchModelClass ?>();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
<?php else: ?>
        $dataProvider = new ActiveDataProvider([
            'query' => <?= $modelClass ?>::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
<?php endif; ?>
    }

    /**
     * Displays a single <?= $modelClass ?> model.
     * <?= implode("\n     * ", $actionParamComments) . "\n" ?>
     * @return mixed
     */
    public function actionView(<?= $actionParams ?>)
    {
        return $this->render('view', [
            'model' => $this->findModel(<?= $actionParams ?>),
        ]);
    }

    /**
     * Creates a new <?= $modelClass ?> model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new <?= $modelClass ?>();
        $scenarios = $model->scenarios();
        isset($scenarios['create']) and $model->setScenario('create');
        $post = Yii::$app->request->post();
        $fileAttributes = $this->getFileAttributes($model);

        if (!empty($fileAttributes) && Yii::$app->request->isPost) {
            $scope = $model->formName();
            foreach ($fileAttributes as $attribute => $type) {
                $filePath = $this->uploadFile($model, $attribute, $type);
                $post[$scope][$attribute] = $filePath ? $filePath : '';
            }
        }
 
        if ($model->load($post) && $model->save()) {
            return $this->redirect(['view', <?= $urlParams ?>]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'fileAttributes' => $fileAttributes,
            ]);
        }
    }

    /**
     * Updates an existing <?= $modelClass ?> model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * <?= implode("\n     * ", $actionParamComments) . "\n" ?>
     * @return mixed
     */
    public function actionUpdate(<?= $actionParams ?>)
    {
        $model = $this->findModel(<?= $actionParams ?>);
        $scenarios = $model->scenarios();
        isset($scenarios['update']) and $model->setScenario('update');
        $post = Yii::$app->request->post();
        $fileAttributes = $this->getFileAttributes($model);

        if (!empty($fileAttributes) && Yii::$app->request->isPost) {
            $scope = $model->formName();
            foreach ($fileAttributes as $attribute => $type) {
                $filePath = $this->uploadFile($model, $attribute, $type);
                $post[$scope][$attribute] = $filePath ? $filePath : $model->$attribute;
            }
        }

        if ($model->load($post) && $model->save()) {
            return $this->redirect(['view', <?= $urlParams ?>]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'fileAttributes' => $fileAttributes,
            ]);
        }
    }

    /**
     * Deletes an existing <?= $modelClass ?> model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * <?= implode("\n     * ", $actionParamComments) . "\n" ?>
     * @return mixed
     */
    public function actionDelete(<?= $actionParams ?>)
    {
        $this->findModel(<?= $actionParams ?>)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the <?= $modelClass ?> model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * <?= implode("\n     * ", $actionParamComments) . "\n" ?>
     * @return <?=                   $modelClass ?> the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel(<?= $actionParams ?>)
    {
<?php
if (count($pks) === 1) {
    $condition = '$id';
} else {
    $condition = [];
    foreach ($pks as $pk) {
        $condition[] = "'$pk' => \$$pk";
    }
    $condition = '[' . implode(', ', $condition) . ']';
}
?>
        if (($model = <?= $modelClass ?>::findOne(<?= $condition ?>)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Get the file attributes of <?= $modelClass . "\n" ?> 
     * @return array file attributes
     */
    protected function getFileAttributes($model)
    {
        $fileAttributes = [];
        if (method_exists ($model, 'filedInputType')) {
            $filedInputType = $model::filedInputType ();
        } else {
            $filedInputType = NULL;
        }
        if (!empty($filedInputType)) {
            foreach ($filedInputType as $attribute => $type) {
                if ($type == 'image' || $type == 'file') {
                    $fileAttributes[$attribute] = $type;
                }   
            }
        }
        return $fileAttributes;
    }

    /**
     * Generates code for active field
     * @param object  of <?= $modelClass . "\n" ?>
     * @param string attribute of <?= $modelClass . "\n" ?>
     * @return string if success or false
     */
    protected function uploadFile($model, $attribute, $type='image')
    {
        $file = UploadedFile::getInstance($model, $attribute);
        if ($file) {
            $fileName = "userfiles/$type/" . date('Y/m/d/'). md5(uniqid(mt_rand(10000, 99999), true)) . '.' . $file->extension;
            $basePath = dirname($fileName);
            if (!file_exists($basePath)) {
                mkdir($basePath, 0755, true);
            }
            $file->saveAs($fileName);
            return $fileName;
        }
        return false;
    }
}
